import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapSDcreenComponent } from './modules/maps/screens/map-sdcreen/map-sdcreen.component'; 

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'maps',
    component: MapSDcreenComponent,
  },
  // {
  //   path: 'dashboard',
  //   component: DashboardComponent,
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: () =>
  //         import('./modules/home/home.module').then((m) => m.HomeModule),
  //     },
  //     {
  //       path: 'user',
  //       loadChildren: () =>
  //         import('./modules/user/user.module').then((m) => m.UserModule),
  //     },
  //   ],
  // },

  {
    path: '**',
    redirectTo: 'auth',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
