import { Component, OnInit } from '@angular/core';
import { DiseaseModel } from '../../models/disease.model';
import { DiseaseService } from '../../services/disease.service';

@Component({
  selector: 'app-table-disease',
  templateUrl: './table-disease.component.html',
  styleUrls: ['./table-disease.component.css'],
})
export class TableDiseaseComponent implements OnInit {
  diseases: DiseaseModel[] = [];
  p: number = 1;
  constructor(private diseaseService: DiseaseService) {
    this.getAll();
  }
  getAll() {
    this.diseaseService.getAll().subscribe((data) => {
      this.diseases = data;
    });
  }
  ngOnInit(): void { }
  ondelete(id: string | undefined) {
    this.diseaseService.delete(id as string).subscribe((data) => {
      this.getAll();
    });
  }
}
