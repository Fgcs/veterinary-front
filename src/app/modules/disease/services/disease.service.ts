import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateDisease } from '../models/createDisease.model';
import { DiseaseModel } from '../models/disease.model';
import { UpdateDisease } from '../models/updateDisease.model';

@Injectable({
  providedIn: 'root',
})
export class DiseaseService {
  constructor(private http: HttpClient) {}
  create(disease: CreateDisease): Observable<DiseaseModel> {
    return this.http.post<DiseaseModel>(
      `${environment.apiUrl}/disease`,
      disease
    );
  }
  getAll(): Observable<DiseaseModel[]> {
    return this.http.get<DiseaseModel[]>(`${environment.apiUrl}/disease`);
  }
  foundDiseaseByName(name: string): Observable<DiseaseModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<DiseaseModel[]>(
      `${environment.apiUrl}/disease/search/find-by-name?name=${name}`,
      { headers }
    );
  }
  getById(id: string): Observable<DiseaseModel> {
    return this.http.get<DiseaseModel>(`${environment.apiUrl}/disease/${id}`);
  }
  delete(id: string): Observable<DiseaseModel> {
    return this.http.delete<DiseaseModel>(
      `${environment.apiUrl}/disease/${id}`
    );
  }
  update(id: string, disease: UpdateDisease): Observable<DiseaseModel> {
    return this.http.patch<DiseaseModel>(
      `${environment.apiUrl}/disease/${id}`,
      disease
    );
  }
}
