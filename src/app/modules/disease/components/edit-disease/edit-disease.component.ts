import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { DiseaseModel } from '../../models/disease.model';
import { UpdateDisease } from '../../models/updateDisease.model';
import { DiseaseService } from '../../services/disease.service';

@Component({
  selector: 'app-edit-disease',
  templateUrl: './edit-disease.component.html',
  styleUrls: ['./edit-disease.component.css'],
})
export class EditDiseaseComponent implements OnInit {
  diseaseForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  diseaseId: string = '';
  disease!: DiseaseModel;
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private diseaseService: DiseaseService,
    private sweetAlert: SweetAlertService,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.diseaseId = this.activated.snapshot.paramMap.get('id') as string;
    this.getDiseaseById();
  }
  getDiseaseById() {
    this.diseaseService.getById(this.diseaseId).subscribe((data) => {
      this.disease = data;
      this.setForm();
    });
  }
  setForm() {
    this.diseaseForm.setValue({
      name: this.disease.name,
      description: this.disease.description,
    });
  }
  get f() {
    return this.diseaseForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.diseaseForm.valid) {
      const body: UpdateDisease = this.diseaseForm.value;
      this.diseaseService.update(this.diseaseId, body).subscribe(
        (resp) => {
          this.sweetAlert
            .succesAlert('Enfermedad Guarda correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/enfermedades/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la enfermdad'
          );
        }
      );
    }
  }
}
