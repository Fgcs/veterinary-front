import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateDisease } from '../../models/createDisease.model';
import { DiseaseService } from '../../services/disease.service';

@Component({
  selector: 'app-create-disease',
  templateUrl: './create-disease.component.html',
  styleUrls: ['./create-disease.component.css'],
})
export class CreateDiseaseComponent implements OnInit {
  diseaseForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private diseaseService: DiseaseService,
    private sweetAlert: SweetAlertService
  ) {}

  ngOnInit(): void {}
  get f() {
    return this.diseaseForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.diseaseForm.valid) {
      const body: CreateDisease = this.diseaseForm.value;
      this.diseaseService.create(body).subscribe(
        (resp) => {
          this.sweetAlert
            .succesAlert('Enfermedad Guarda correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/enfermedades/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la enfermdad'
          );
        }
      );
    }
  }
}
