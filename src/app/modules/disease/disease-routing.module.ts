import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateDiseaseComponent } from './components/create-disease/create-disease.component';
import { EditDiseaseComponent } from './components/edit-disease/edit-disease.component';
import { ListDiseaseComponent } from './components/list-disease/list-disease.component';

const routes: Routes = [
  {
    path: 'listado',
    component: ListDiseaseComponent,
  },
  {
    path: 'agregar',
    component: CreateDiseaseComponent,
  },
  {
    path: 'editar/:id',
    component: EditDiseaseComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiseaseRoutingModule {}
