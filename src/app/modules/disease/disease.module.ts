import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiseaseRoutingModule } from './disease-routing.module';
import { CreateDiseaseComponent } from './components/create-disease/create-disease.component';
import { EditDiseaseComponent } from './components/edit-disease/edit-disease.component';
import { ListDiseaseComponent } from './components/list-disease/list-disease.component';
import { TableDiseaseComponent } from './shared/table-disease/table-disease.component';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { RouterModule } from '@angular/router';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    CreateDiseaseComponent,
    EditDiseaseComponent,
    ListDiseaseComponent,
    TableDiseaseComponent,
  ],
  imports: [
    CommonModule,
    DiseaseRoutingModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    FormsModule,
    ReactiveFormsModule, NgxPaginationModule
  ],
})
export class DiseaseModule { }
