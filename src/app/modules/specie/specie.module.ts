import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecieRoutingModule } from './specie-routing.module';
import { CreateSpecieComponent } from './components/create-specie/create-specie.component';
import { ListSpecieComponent } from './components/list-specie/list-specie.component';
import { EditSpecieComponent } from './components/edit-specie/edit-specie.component';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { RouterModule } from '@angular/router';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { TableSpecieComponent } from './shared/table-specie/table-specie.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    CreateSpecieComponent,
    ListSpecieComponent,
    EditSpecieComponent,
    TableSpecieComponent,
  ],
  imports: [
    CommonModule,
    SpecieRoutingModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
})
export class SpecieModule { }
