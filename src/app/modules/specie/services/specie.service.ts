import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateSpecie } from '../model/createSpecie.model';
import { SpecieModel } from '../model/specie.model';
import { UpdateSpecie } from '../model/updateSpecie.model';

@Injectable({
  providedIn: 'root',
})
export class SpecieService {
  constructor(private http: HttpClient) {}
  create(specie: CreateSpecie): Observable<SpecieModel> {
    return this.http.post<SpecieModel>(`${environment.apiUrl}/specie`, specie);
  }
  getAll(): Observable<SpecieModel[]> {
    return this.http.get<SpecieModel[]>(`${environment.apiUrl}/specie`);
  }
  getById(id: string): Observable<SpecieModel> {
    return this.http.get<SpecieModel>(`${environment.apiUrl}/specie/${id}`);
  }
  delete(id: string): Observable<SpecieModel> {
    return this.http.delete<SpecieModel>(`${environment.apiUrl}/specie/${id}`);
  }
  update(id: string, specie: UpdateSpecie): Observable<SpecieModel> {
    return this.http.patch<SpecieModel>(
      `${environment.apiUrl}/specie/${id}`,
      specie
    );
  }
  foundSpecieByName(name: string): Observable<SpecieModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<SpecieModel[]>(
      `${environment.apiUrl}/specie/search/find-by-name?name=${name}`,
      { headers }
    );
  }
}
