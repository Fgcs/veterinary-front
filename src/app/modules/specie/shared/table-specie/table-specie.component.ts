import { Component, Input, OnInit } from '@angular/core';
import { SpecieModel } from '../../model/specie.model';
import { SpecieService } from '../../services/specie.service';

@Component({
  selector: 'app-table-specie',
  templateUrl: './table-specie.component.html',
  styleUrls: ['./table-specie.component.css'],
})
export class TableSpecieComponent implements OnInit {
  @Input() species: SpecieModel[] = [];
  p: number = 1;
  constructor(private specieService: SpecieService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.specieService.getAll().subscribe((data) => {
      this.species = data;
    });
  }
  ondelete(id: string | undefined) {
    this.specieService.delete(id as string).subscribe((dta) => {
      this.getAll();
    });
  }
}
