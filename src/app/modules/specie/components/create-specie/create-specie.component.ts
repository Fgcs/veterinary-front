import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateSpecie } from '../../model/createSpecie.model';
import { SpecieService } from '../../services/specie.service';

@Component({
  selector: 'app-create-specie',
  templateUrl: './create-specie.component.html',
  styleUrls: ['./create-specie.component.css'],
})
export class CreateSpecieComponent implements OnInit {
  specieForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private specieService: SpecieService,
    private sweetAlert: SweetAlertService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  get f() {
    return this.specieForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.specieForm.valid) {
      const specie: CreateSpecie = this.specieForm.value;
      this.specieService.create(specie).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Especie Guardada correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/especies/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la especie'
          );
        }
      );
    }
  }
}
