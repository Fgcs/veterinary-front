import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSpecieComponent } from './list-specie.component';

describe('ListSpecieComponent', () => {
  let component: ListSpecieComponent;
  let fixture: ComponentFixture<ListSpecieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSpecieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSpecieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
