import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { SpecieModel } from '../../model/specie.model';
import { UpdateSpecie } from '../../model/updateSpecie.model';
import { SpecieService } from '../../services/specie.service';

@Component({
  selector: 'app-edit-specie',
  templateUrl: './edit-specie.component.html',
  styleUrls: ['./edit-specie.component.css'],
})
export class EditSpecieComponent implements OnInit {
  specieForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  submitted: boolean = false;
  specie!: SpecieModel;
  specieId: string = '';
  constructor(
    private fb: FormBuilder,
    private specieService: SpecieService,
    private sweetAlert: SweetAlertService,
    private router: Router,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.specieId = this.activated.snapshot.paramMap.get('id') as string;
    this.getSpecieById();
  }
  getSpecieById() {
    this.specieService.getById(this.specieId).subscribe((dta) => {
      this.specie = dta;
      this.setForm();
    });
  }
  setForm() {
    this.specieForm.setValue({
      name: this.specie.name,
      description: this.specie.description,
    });
  }
  get f() {
    return this.specieForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.specieForm.valid) {
      const specie: UpdateSpecie = this.specieForm.value;
      this.specieService.update(this.specieId, specie).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Especie Guardada correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/especies/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la especie'
          );
        }
      );
    }
  }
}
