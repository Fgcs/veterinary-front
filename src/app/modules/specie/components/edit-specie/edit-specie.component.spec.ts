import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpecieComponent } from './edit-specie.component';

describe('EditSpecieComponent', () => {
  let component: EditSpecieComponent;
  let fixture: ComponentFixture<EditSpecieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSpecieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpecieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
