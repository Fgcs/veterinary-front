import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateSpecieComponent } from './components/create-specie/create-specie.component';
import { EditSpecieComponent } from './components/edit-specie/edit-specie.component';
import { ListSpecieComponent } from './components/list-specie/list-specie.component';

const routes: Routes = [
  {
    path: 'listado',
    component: ListSpecieComponent,
  },
  {
    path: 'agregar',
    component: CreateSpecieComponent,
  },
  {
    path: 'editar/:id',
    component: EditSpecieComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecieRoutingModule {}
