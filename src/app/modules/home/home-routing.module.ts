import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { MapSDcreenComponent } from '../maps/screens/map-sdcreen/map-sdcreen.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'users',
        loadChildren: () =>
          import('../user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'animal',
        loadChildren: () =>
          import('../animal/animal.module').then((m) => m.AnimalModule),
      },
      {
        path: 'especies',
        loadChildren: () =>
          import('../specie/specie.module').then((m) => m.SpecieModule),
      },
      {
        path: 'razas',
        loadChildren: () =>
          import('../breed/breed.module').then((m) => m.BreedModule),
      },
      {
        path: 'vacunas',
        loadChildren: () =>
          import('../vaccine/vaccine.module').then((m) => m.VaccineModule),
      },
      {
        path: 'enfermedades',
        loadChildren: () =>
          import('../disease/disease.module').then((m) => m.DiseaseModule),
      },
      {
        path: 'cuidados',
        loadChildren: () =>
          import('../care/care.module').then((m) => m.CareModule),
      },
      {
        path: 'maps',
        component: MapSDcreenComponent,
      },
      {
        path: '**',
        redirectTo: 'users',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
