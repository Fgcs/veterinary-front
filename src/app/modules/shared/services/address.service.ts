import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AddressModel } from '../../animal/models/address.model';
import { CreateAddress } from '../../animal/models/createAddress.model';

@Injectable({
  providedIn: 'root',
})
export class AddressService {
  constructor(private http: HttpClient) {}
  create(address: CreateAddress): Observable<AddressModel> {
    return this.http.post<AddressModel>(
      `${environment.apiUrl}/address`,
      address
    );
  }
}
