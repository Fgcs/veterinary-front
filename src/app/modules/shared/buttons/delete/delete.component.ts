import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';

@Component({
  selector: 'delete-button',
  template: `
    <button class="btn btn-danger mx-1" (click)="onDelete()">
      <i class="fa-solid fa-trash"></i> borrar
    </button>
  `,
  styleUrls: ['./delete.component.css'],
})
export class DeleteComponent implements OnInit {
  @Input() text: string = '';
  @Output() deleteEvent = new EventEmitter();

  constructor(private sweetAlert: SweetAlertService) {}

  ngOnInit(): void {}
  onDelete() {
    this.sweetAlert
      .deleteAlert(`Estas seguro que deseas borrar esta ${this.text}?`)
      .then((result) => {
        if (result.isConfirmed) {
          this.deleteEvent.emit(true);
        }
      });
  }
}
