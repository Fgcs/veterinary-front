export class CreateUser {
  name!: string;

  lastName!: string;

  dni!: string;
  phone!: string;

  email!: string;

  username!: string;

  password!: string;

  rol!: string;
}
