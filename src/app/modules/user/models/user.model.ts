export class UserModel {
  id?: string;
  name!: string;
  lastName!: string;
  dni!: string;
  phone!: string;
  //enum!: string
  rol!: string;
  email!: string;
  username!: string;
  password!: string;
  created_at?: Date;
  updated_at?: Date;
}
