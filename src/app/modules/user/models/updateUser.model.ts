export class UpdateUser {
  name!: string;

  lastName!: string;

  dni!: string;
  phone!: string;

  email!: string;

  username!: string;

  rol!: string;
}
