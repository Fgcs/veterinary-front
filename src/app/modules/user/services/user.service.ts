import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateRescuer } from '../models/create.rescuer.model';
import { UpdateUser } from '../models/updateUser.model';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getBydni(dni: string): Observable<UserModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<UserModel[]>(
      `${environment.apiUrl}/user/search/find-by-dni?dni=${dni}`,
      { headers }
    );
  }
  getAll(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(`${environment.apiUrl}/user`);
  }
  createRescuer(rescuer: CreateRescuer): Observable<UserModel> {
    return this.http.post<UserModel>(
      `${environment.apiUrl}/user/rescuer`,
      rescuer
    );
  }
  delete(id: string): Observable<UserModel> {
    return this.http.delete<UserModel>(`${environment.apiUrl}/user/${id}`);
  }
  getById(id: string): Observable<UserModel> {
    return this.http.get<UserModel>(`${environment.apiUrl}/user/${id}`);
  }
  update(id: string, user: UpdateUser): Observable<UserModel> {
    return this.http.put<UserModel>(`${environment.apiUrl}/user/${id}`, user);
  }
}
