import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsAnimalComponent } from '../animal/components/details-animal/details-animal.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { DetailsUserComponent } from './components/details-user/details-user.component';
import { PossibleOwnerComponent } from './components/possible-owner/possible-owner.component';
import { SupervisorEvaluationComponent } from './components/supervisor-evaluation/supervisor-evaluation.component';
import { SupervisorVisitComponent } from './components/supervisor-visit/supervisor-visit.component';
import { VeterinaryConsultationComponent } from './components/veterinary-consultation/veterinary-consultation.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { WorkingUsersComponent } from './components/working-users/working-users.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
  },
  {
    path: 'listado',
    component: UserListComponent,
  },

  {
    path: 'possible-owner',
    component: PossibleOwnerComponent,
  },
  {
    path: 'working-users',
    component: WorkingUsersComponent,
  },
  {
    path: 'consultation',
    component: VeterinaryConsultationComponent,
  },
  {
    path: 'visit',
    component: SupervisorVisitComponent,
  },
  {
    path: 'evaluation',
    component: SupervisorEvaluationComponent,
  },
  {
    path: 'create',
    component: CreateUserComponent,
  },
  {
    path: 'editar/:id',
    component: EditUserComponent,
  },
  {
    path: 'detalles/:id',
    component: DetailsUserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
