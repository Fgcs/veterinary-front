import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { PossibleOwnerComponent } from './components/possible-owner/possible-owner.component';
import { WorkingUsersComponent } from './components/working-users/working-users.component';
import { VeterinaryConsultationComponent } from './components/veterinary-consultation/veterinary-consultation.component';
import { SupervisorEvaluationComponent } from './components/supervisor-evaluation/supervisor-evaluation.component';
import { SupervisorVisitComponent } from './components/supervisor-visit/supervisor-visit.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule } from '@angular/material/stepper';
import { TableUserComponent } from './shared/table-user/table-user.component';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { DetailsUserComponent } from './components/details-user/details-user.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    UserListComponent,
    PossibleOwnerComponent,
    WorkingUsersComponent,
    VeterinaryConsultationComponent,
    SupervisorEvaluationComponent,
    SupervisorVisitComponent,
    WelcomeComponent,
    CreateUserComponent,
    TableUserComponent,
    DetailsUserComponent,
    EditUserComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule,
    DeleteModule,
    TitlePageModule,
    MatExpansionModule,
    NgxPaginationModule,
  ],
})
export class UserModule { }
