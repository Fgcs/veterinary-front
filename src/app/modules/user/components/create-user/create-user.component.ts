import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { DniValidatorService } from 'src/app/shared/services/dni-validator.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { UsernameValidatorService } from 'src/app/shared/services/username-validator.service';
import { CreateUser } from '../../models/createUser.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
})
export class CreateUserComponent implements OnInit {
  userForm: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    dni: ['', Validators.required, [this.dniValidator]],
    phone: [''],
    rol: [null, Validators.required],
    username: ['', Validators.required, [this.usernameValidator]],
    email: ['', Validators.required],
    password: ['12345678', Validators.required],
  });
  submitted: boolean = false;
  isEditable = true;
  constructor(
    private _formBuilder: FormBuilder,
    private dniValidator: DniValidatorService,
    private usernameValidator: UsernameValidatorService,
    private authService: AuthService,
    private router: Router,
    private sweetAlert: SweetAlertService
  ) {}

  ngOnInit(): void {}
  onSubmit() {
    this.submitted = true;
    if (this.userForm.valid) {
      const user: CreateUser = this.userForm.value;
      this.authService.register(user).subscribe((resp) => {
        this.sweetAlert
          .succesAlert('usuario Guardo correctamente')
          .then((end) => {
            this.router.navigateByUrl('/dashboard/users/listado');
          });
      });
    }
  }
  get f() {
    return this.userForm.controls;
  }
}
