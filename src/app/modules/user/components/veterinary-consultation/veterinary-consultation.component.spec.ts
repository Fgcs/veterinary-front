import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VeterinaryConsultationComponent } from './veterinary-consultation.component';

describe('VeterinaryConsultationComponent', () => {
  let component: VeterinaryConsultationComponent;
  let fixture: ComponentFixture<VeterinaryConsultationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VeterinaryConsultationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VeterinaryConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
