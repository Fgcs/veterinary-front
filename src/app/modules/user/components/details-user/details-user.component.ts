import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.css'],
})
export class DetailsUserComponent implements OnInit {
  userId: string = '';
  user!: UserModel;
  constructor(
    private userService: UserService,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.userId = this.activated.snapshot.paramMap.get('id') as string;
    this.getUserById();
  }
  getUserById() {
    this.userService.getById(this.userId).subscribe((data) => {
      this.user = data;
    });
  }
}
