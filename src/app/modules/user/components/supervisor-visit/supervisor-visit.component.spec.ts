import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorVisitComponent } from './supervisor-visit.component';

describe('SupervisorVisitComponent', () => {
  let component: SupervisorVisitComponent;
  let fixture: ComponentFixture<SupervisorVisitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupervisorVisitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
