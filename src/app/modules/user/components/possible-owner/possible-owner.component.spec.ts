import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PossibleOwnerComponent } from './possible-owner.component';

describe('PossibleOwnerComponent', () => {
  let component: PossibleOwnerComponent;
  let fixture: ComponentFixture<PossibleOwnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PossibleOwnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PossibleOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
