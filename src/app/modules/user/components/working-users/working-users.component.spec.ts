import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingUsersComponent } from './working-users.component';

describe('WorkingUsersComponent', () => {
  let component: WorkingUsersComponent;
  let fixture: ComponentFixture<WorkingUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkingUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
