import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { DniValidatorService } from 'src/app/shared/services/dni-validator.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { UsernameValidatorService } from 'src/app/shared/services/username-validator.service';
import { CreateUser } from '../../models/createUser.model';
import { UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
})
export class EditUserComponent implements OnInit {
  userForm: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    dni: ['', Validators.required, [this.dniValidator]],
    phone: [''],
    rol: [null, Validators.required],
    username: ['', Validators.required, [this.usernameValidator]],
    email: ['', Validators.required],
  });
  submitted: boolean = false;
  isEditable = true;
  userId: string = '';
  user!: UserModel;
  constructor(
    private _formBuilder: FormBuilder,
    private dniValidator: DniValidatorService,
    private usernameValidator: UsernameValidatorService,
    private router: Router,
    private sweetAlert: SweetAlertService,
    private activated: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userId = this.activated.snapshot.paramMap.get('id') as string;
    this.getUserById();
  }
  getUserById() {
    this.userService.getById(this.userId).subscribe((data) => {
      this.user = data;
      this.setData();
    });
  }
  setData() {
    this.userForm.setValue({
      name: this.user.name,
      lastName: this.user.lastName,
      dni: this.user.dni,
      phone: this.user.phone,
      rol: this.user.rol,
      username: this.user.username,
      email: this.user.email,
    });
  }
  onSubmit() {
    this.submitted = true;
    if (this.userForm.valid) {
      const user: CreateUser = this.userForm.value;
      this.userService.update(this.userId, user).subscribe((resp) => {
        this.sweetAlert
          .succesAlert('usuario Guardo correctamente')
          .then((end) => {
            this.router.navigateByUrl('/dashboard/users/listado');
          });
      });
    }
  }
  get f() {
    return this.userForm.controls;
  }
}
