import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-table-user',
  templateUrl: './table-user.component.html',
  styleUrls: ['./table-user.component.css'],
})
export class TableUserComponent implements OnInit {
  users: UserModel[] = [];
  p: number = 1;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getAllUser();
  }
  getAllUser() {
    this.userService.getAll().subscribe((data) => {
      this.users = data;
    });
  }
  ondelete(id: string | undefined) {
    this.userService.delete(id as string).subscribe((data) => {
      this.getAllUser();
    });
  }
}
