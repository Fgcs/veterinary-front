import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCareComponent } from './table-care.component';

describe('TableCareComponent', () => {
  let component: TableCareComponent;
  let fixture: ComponentFixture<TableCareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableCareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
