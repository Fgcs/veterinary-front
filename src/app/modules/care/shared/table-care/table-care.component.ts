import { Component, OnInit } from '@angular/core';
import { CareModel } from '../../models/care.models';
import { CareService } from '../../services/care.service';

@Component({
  selector: 'app-table-care',
  templateUrl: './table-care.component.html',
  styleUrls: ['./table-care.component.css'],
})
export class TableCareComponent implements OnInit {
  cares: CareModel[] = []; p: number = 1;
  constructor(private careService: CareService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.careService.getAll().subscribe((data) => {
      this.cares = data;
    });
  }
  ondelete(id: string | undefined) {
    this.careService.delete(id as string).subscribe((data) => {
      this.getAll();
    });
  }
}
