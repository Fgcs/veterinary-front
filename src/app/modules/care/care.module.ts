import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CareRoutingModule } from './care-routing.module';
import { CreateCareComponent } from './components/create-care/create-care.component';
import { EditCareComponent } from './components/edit-care/edit-care.component';
import { ListCareComponent } from './components/list-care/list-care.component';
import { TableCareComponent } from './shared/table-care/table-care.component';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { RouterModule } from '@angular/router';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    CreateCareComponent,
    EditCareComponent,
    ListCareComponent,
    TableCareComponent,
  ],
  imports: [
    CommonModule,
    CareRoutingModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    FormsModule,
    ReactiveFormsModule, NgxPaginationModule
  ],
})
export class CareModule { }
