import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCareComponent } from './components/create-care/create-care.component';
import { EditCareComponent } from './components/edit-care/edit-care.component';
import { ListCareComponent } from './components/list-care/list-care.component';

const routes: Routes = [
  {
    path: 'listado',
    component: ListCareComponent,
  },
  {
    path: 'agregar',
    component: CreateCareComponent,
  },
  {
    path: 'editar/:id',
    component: EditCareComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CareRoutingModule {}
