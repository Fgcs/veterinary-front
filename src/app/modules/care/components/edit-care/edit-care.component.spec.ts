import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCareComponent } from './edit-care.component';

describe('EditCareComponent', () => {
  let component: EditCareComponent;
  let fixture: ComponentFixture<EditCareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
