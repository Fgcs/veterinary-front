import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CareModel } from '../../models/care.models';
import { UpdateCare } from '../../models/updateCare.model';
import { CareService } from '../../services/care.service';

@Component({
  selector: 'app-edit-care',
  templateUrl: './edit-care.component.html',
  styleUrls: ['./edit-care.component.css'],
})
export class EditCareComponent implements OnInit {
  careForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  submitted: boolean = false;
  careId: string = '';
  care!: CareModel;
  constructor(
    private fb: FormBuilder,
    private careService: CareService,
    private sweetAlert: SweetAlertService,
    private router: Router,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.careId = this.activated.snapshot.paramMap.get('id') as string;
    this.getCareById();
  }
  getCareById() {
    this.careService.getById(this.careId).subscribe((data) => {
      this.care = data;
      this.setForm();
    });
  }
  setForm() {
    this.careForm.setValue({
      name: this.care.name,
      description: this.care.description,
    });
  }
  get f() {
    return this.careForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.careForm.valid) {
      const care: UpdateCare = this.careForm.value;
      this.careService.update(this.careId, care).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Cuidado Guardado correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/cuidados/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar este cuidado'
          );
        }
      );
    }
  }
}
