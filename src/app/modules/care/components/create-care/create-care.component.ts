import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateCare } from '../../models/createCare.model';
import { CareService } from '../../services/care.service';

@Component({
  selector: 'app-create-care',
  templateUrl: './create-care.component.html',
  styleUrls: ['./create-care.component.css'],
})
export class CreateCareComponent implements OnInit {
  careForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private careService: CareService,
    private sweetAlert: SweetAlertService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  get f() {
    return this.careForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.careForm.valid) {
      const care: CreateCare = this.careForm.value;
      this.careService.create(care).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Cuidado Guardado correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/cuidados/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar este cuidado'
          );
        }
      );
    }
  }
}
