import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCareComponent } from './list-care.component';

describe('ListCareComponent', () => {
  let component: ListCareComponent;
  let fixture: ComponentFixture<ListCareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
