import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CareModel } from '../models/care.models';
import { CreateCare } from '../models/createCare.model';
import { UpdateCare } from '../models/updateCare.model';

@Injectable({
  providedIn: 'root',
})
export class CareService {
  constructor(private http: HttpClient) {}
  create(care: CreateCare): Observable<CareModel> {
    return this.http.post<CareModel>(`${environment.apiUrl}/care`, care);
  }
  getAll(): Observable<CareModel[]> {
    return this.http.get<CareModel[]>(`${environment.apiUrl}/care`);
  }
  getById(id: string): Observable<CareModel> {
    return this.http.get<CareModel>(`${environment.apiUrl}/care/${id}`);
  }
  foundCareByName(name: string): Observable<CareModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<CareModel[]>(
      `${environment.apiUrl}/care/search/find-by-name?name=${name}`,
      { headers }
    );
  }
  delete(id: string): Observable<CareModel> {
    return this.http.delete<CareModel>(`${environment.apiUrl}/care/${id}`);
  }
  update(id: string, care: UpdateCare): Observable<CareModel> {
    return this.http.patch<CareModel>(`${environment.apiUrl}/care/${id}`, care);
  }
}
