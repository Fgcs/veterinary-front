import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-animal',
  templateUrl: './edit-animal.component.html',
  styleUrls: ['./edit-animal.component.css'],
})
export class EditAnimalComponent implements OnInit {
  firstFormGroup: FormGroup = this._formBuilder.group({
    nombreAnimal: ['', Validators.required],
    sexoAnimal: ['', Validators.required],
    colorPeloAnimal: ['', Validators.required],
    fechaNacimiento: ['', Validators.required],
    pesoAnimal: ['', Validators.required],
    especieAnimal: ['', Validators.required],
    razaAnimal: ['', Validators.required],
  });

  secondFormGroup: FormGroup = this._formBuilder.group({
    estadoRescate: ['', Validators.required],
    ciudadRescate: ['', Validators.required],
    calleRescate: ['', Validators.required],
    NumeroCasa: ['', Validators.required],
    codigoPostal: ['', Validators.required],
    lugarReferencia: ['', Validators.required],
    fechaRescate: ['', Validators.required],
  });

  thirdFormGroup: FormGroup = this._formBuilder.group({
    nombreRescatista: ['', Validators.required],
    apellidoRescatista: ['', Validators.required],
    cedularescatista: ['', Validators.required],
    numeroRescatista: ['', Validators.required],
    correoRescatista: ['', Validators.required],
  });
  isEditable = true;
  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit(): void {}
}
