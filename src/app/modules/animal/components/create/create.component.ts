import { Component, EventEmitter, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  debounceTime,
  filter,
  finalize,
  lastValueFrom,
  map,
  Observable,
  startWith,
  switchMap,
} from 'rxjs';
import { BreedModel } from 'src/app/modules/breed/models/breed.model';
import { BreedService } from 'src/app/modules/breed/services/breed.service';
import { locationModel } from 'src/app/modules/maps/interfaces/location';
import { AddressService } from 'src/app/modules/shared/services/address.service';
import { SpecieModel } from 'src/app/modules/specie/model/specie.model';
import { SpecieService } from 'src/app/modules/specie/services/specie.service';
import { CreateRescuer } from 'src/app/modules/user/models/create.rescuer.model';
import { UserModel } from 'src/app/modules/user/models/user.model';
import { UserService } from 'src/app/modules/user/services/user.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { LocationModel } from '../../models/address.model';
import { CreateAddress } from '../../models/createAddress.model';
import { CreateAnimal } from '../../models/createAnimal.molde';
import { CreateRescue } from '../../models/createRescue.model';
import { RescueModel } from '../../models/rescue.model';
import { AnimalService } from '../../services/animal.service';
import { RescueService } from '../../services/rescue.service';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  animalForm: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    gender: ['', Validators.required],
    hair_color: ['', Validators.required],
    weight: ['', Validators.required],
    birthday: ['', Validators.required],
    specieId: ['', Validators.required],
    breedId: ['', Validators.required],
  });

  addressForm: FormGroup = this._formBuilder.group({
    state: ['', Validators.required],
    city: ['', Validators.required],
    street: ['', Validators.required],
    home: ['', Validators.required],
    postal_code: ['', Validators.required],
  });
  rescuerForm: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    dni: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', Validators.required],
    id: null,
  });
  myControl = new FormControl();
  filteredOptions: Observable<UserModel[]> | undefined;
  submitted: boolean = false;
  foundSpecie = new EventEmitter<string>();
  keyword = 'name';
  species: SpecieModel[] = [];
  breeds: BreedModel[] = [];
  location!: locationModel;
  hasValue: boolean = false;
  constructor(
    private _formBuilder: FormBuilder,
    private specieSvc: SpecieService,
    private breedService: BreedService,
    private userService: UserService,
    private animalService: AnimalService,
    private addressService: AddressService,
    private rescueService: RescueService,
    private sweetAlert: SweetAlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      switchMap((value) => this.userService.getBydni(value))
    );
  }

  get formAnimal() {
    return this.animalForm.controls;
  }
  get formAddress() {
    return this.addressForm.controls;
  }
  get formRescuer() {
    return this.rescuerForm.controls;
  }
  isValid() {
    this.submitted = this.animalForm.valid ? false : true;
  }
  addressValid() {
    this.submitted = this.addressForm.valid ? false : true;
  }
  searchSpecies(name: string) {
    this.specieSvc.foundSpecieByName(name).subscribe((data) => {
      this.species = data;
    });
  }
  searchBreed(name: string) {
    this.breedService.foundBreedByName(name).subscribe((data) => {
      this.breeds = data;
    });
  }
  getLocation(location: locationModel) {
    this.location = location;
  }
  selectUser(event: any) {
    this.hasValue = true;
    const user = event.option.value;
    this.rescuerForm.patchValue({
      name: user.name,
      lastName: user.lastName,
      dni: user.dni,
      phone: user.phone,
      email: user.email ? user.email : 'no tiene debe hablar con soporte',
      id: user.id,
    });
  }
  clear() {
    this.hasValue = false;
    this.rescuerForm.patchValue({
      name: null,
      lastName: null,
      dni: null,
      phone: null,
      email: null,
      id: null,
    });
    this.myControl.patchValue(null);
  }
  displayFn(user: UserModel): string {
    return user && user.name ? `${user.name} ${user.lastName} ` : '';
  }
  async saveAll() {
    this.submitted = true;
    if (this.rescuerForm.valid) {
      const specie = this.animalForm.value.specieId;
      const breed = this.animalForm.value.breedId;
      const animal: CreateAnimal = {
        ...this.animalForm.value,
        specieId: specie.id,
        breedId: breed.id,
      };
      let address: CreateAddress = this.addressForm.value;
      address.location = this.location;
      const user: CreateRescuer = this.rescuerForm.value;

      const rescue: CreateRescue = {
        animal: animal,
        address: address,
        user: user,
      };
      this.rescueService.create(rescue).subscribe(
        (data) => {
          console.log(data);

          this.sweetAlert
            .succesAlert('Animal Guardado correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/animal/list');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar el animal'
          );
        }
      );
    }
  }
}
