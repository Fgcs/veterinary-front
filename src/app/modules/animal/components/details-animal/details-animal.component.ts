import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnimalModel } from '../../models/animal.model';
import { ConsultationModel } from '../../models/consultation.model';
import { AnimalService } from '../../services/animal.service';
import { ConsultationService } from '../../services/consultation.service';

@Component({
  selector: 'app-details-animal',
  templateUrl: './details-animal.component.html',
  styleUrls: ['./details-animal.component.css'],
})
export class DetailsAnimalComponent implements OnInit {
  animalId: string = '';
  animal!: AnimalModel;
  consultations: ConsultationModel[] = [];
  constructor(
    private animalService: AnimalService,
    private activated: ActivatedRoute,
    private consultationService: ConsultationService
  ) {}

  ngOnInit(): void {
    this.animalId = this.activated.snapshot.paramMap.get('id') as string;
    this.getAnimalById();
  }
  getAnimalById() {
    this.animalService.getById(this.animalId).subscribe((data) => {
      this.animal = data;
      this.getByAnimalId();
    });
  }
  getByAnimalId() {
    this.consultationService
      .getByAnimalId(this.animalId as string)
      .subscribe((data) => {
        this.consultations = data;
      });
  }
}
