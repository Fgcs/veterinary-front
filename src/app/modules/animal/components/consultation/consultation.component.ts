import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { CareModel } from 'src/app/modules/care/models/care.models';
import { CareService } from 'src/app/modules/care/services/care.service';
import { DiseaseModel } from 'src/app/modules/disease/models/disease.model';
import { DiseaseService } from 'src/app/modules/disease/services/disease.service';
import { UserModel } from 'src/app/modules/user/models/user.model';
import { VaccineModel } from 'src/app/modules/vaccine/models/vaccine.model';
import { VaccineService } from 'src/app/modules/vaccine/services/vaccine.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateConsultation } from '../../models/createConsultation.model';
import { ConsultationService } from '../../services/consultation.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css'],
})
export class ConsultationComponent implements OnInit {
  form: FormGroup = this.fb.group({
    weight: ['', Validators.required],
    date: [],
    observations: ['', Validators.required],
  });
  animalId: string = '';
  careControl = new FormControl();
  diseaseControl = new FormControl();
  vaccineControl = new FormControl();
  careOptions: Observable<CareModel[]> | undefined;
  diseaseOptions: Observable<DiseaseModel[]> | undefined;
  vaccineOptions: Observable<VaccineModel[]> | undefined;
  cares: CareModel[] = [];
  diseases: DiseaseModel[] = [];
  vaccines: VaccineModel[] = [];
  user!: UserModel;
  submitted: boolean = false;
  constructor(
    private activated: ActivatedRoute,
    private fb: FormBuilder,
    private careService: CareService,
    private diseaseService: DiseaseService,
    private vaccineService: VaccineService,
    private authService: AuthService,
    private consultationSvc: ConsultationService,
    private router: Router,
    private sweetAlert: SweetAlertService
  ) {}

  ngOnInit(): void {
    this.animalId = this.activated.snapshot.paramMap.get('id') as string;
    this.user = this.authService.getUser();
    this.getCare();
    this.getVaccine();
    this.getDisease();
  }
  getCare() {
    this.careOptions = this.careControl.valueChanges.pipe(
      switchMap((value) => this.careService.foundCareByName(value))
    );
  }
  getDisease() {
    this.diseaseOptions = this.diseaseControl.valueChanges.pipe(
      switchMap((value) => this.diseaseService.foundDiseaseByName(value))
    );
  }
  getVaccine() {
    this.vaccineOptions = this.vaccineControl.valueChanges.pipe(
      switchMap((value) => this.vaccineService.foundVaccineByName(value))
    );
  }
  selectCare(event: any) {
    this.careControl.patchValue(null);
    this.cares.push(event.option.value);
  }
  removeCare(care: CareModel) {
    this.cares = this.cares.filter((item) => item.id !== care.id);
  }
  selectDisease(event: any) {
    this.diseaseControl.patchValue(null);
    this.diseases.push(event.option.value);
  }
  selectVaccine(event: any) {
    this.vaccineControl.patchValue(null);
    this.vaccines.push(event.option.value);
  }
  removeDisease(data: DiseaseModel) {
    this.diseases = this.diseases.filter((item) => item.id !== data.id);
  }
  removeVaccine(data: VaccineModel) {
    this.vaccines = this.vaccines.filter((item) => item.id !== data.id);
  }
  get f() {
    return this.form.controls;
  }
  onSave() {
    this.submitted = true;
    if (this.form.valid) {
      const consultation: CreateConsultation = {
        ...this.form.value,
        cares: this.cares ? this.cares.map((item) => item.id) : [],
        diseases: this.diseases ? this.diseases.map((item) => item.id) : [],
        vaccines: this.vaccines ? this.vaccines.map((item) => item.id) : [],
        animalId: this.animalId,
        doctorId: this.user.id,
      };
      this.consultationSvc.create(consultation).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Consulta medica Guardada correctamente')
            .then((end) => {
              this.router.navigateByUrl(
                `/dashboard/animal/detalles/${this.animalId}`
              );
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar esta consulta medica'
          );
        }
      );
    }
  }
}
