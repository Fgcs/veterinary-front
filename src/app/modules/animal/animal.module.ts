import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatStepperModule } from '@angular/material/stepper';
import { AnimalRoutingModule } from './animal-routing.module';
import { CreateComponent } from './components/create/create.component';
import { DeleteComponent } from './components/delete/delete.component';
import { ListComponent } from './components/list/list.component';
import { AdoptionComponent } from './components/adoption/adoption.component';
import { ClinicHistoryComponent } from './components/clinic-history/clinic-history.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { RouterModule } from '@angular/router';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { TableAnimalComponent } from './shared/table-animal/table-animal.component';
import { EditAnimalComponent } from './components/edit-animal/edit-animal.component';
import { DetailsAnimalComponent } from './components/details-animal/details-animal.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { ConsultationComponent } from './components/consultation/consultation.component';
import { ListHistoryComponent } from './shared/list-history/list-history.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { MapsModule } from '../maps/maps.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  declarations: [
    CreateComponent,
    DeleteComponent,
    ListComponent,
    AdoptionComponent,
    ClinicHistoryComponent,
    TableAnimalComponent,
    EditAnimalComponent,
    DetailsAnimalComponent,
    ConsultationComponent,
    ListHistoryComponent,
  ],
  imports: [
    CommonModule,
    AnimalRoutingModule,
    NgxPaginationModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    MatExpansionModule,
    AutocompleteLibModule,
    MapsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
  ],
})
export class AnimalModule { }
