export class AddressModel {
  id?: string;
  street!: string;
  city!: string;
  state!: string;
  postal_code!: string;
  home!: string;
  reference!: string;
  location?: LocationModel;
  created_at?: Date;
  updated_at?: Date;
}
export class LocationModel {
  id?: string;
  latitude!: string;
  longitude!: string;
}
