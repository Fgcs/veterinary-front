import { BreedModel } from '../../breed/models/breed.model';

export class AnimalModel {
  id?: string;
  name!: string;
  weight!: number;
  gender!: string;
  hair_color!: string;
  record!:string;
  birthday!: Date;
  breed!: BreedModel;
  specie!: AnimalModel;
  created_at?: Date;
  updated_at?: Date;
}
