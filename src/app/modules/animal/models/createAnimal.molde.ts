export class CreateAnimal {
  name!: string;
  weight!: number;
  gender!: string;
  hair_color!: string;
  birthday!: Date;
  breedId!: string;
  specieId!: string;
}
