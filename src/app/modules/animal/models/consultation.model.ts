import { CareModel } from '../../care/models/care.models';
import { DiseaseModel } from '../../disease/models/disease.model';
import { UserModel } from '../../user/models/user.model';
import { VaccineModel } from '../../vaccine/models/vaccine.model';
import { AnimalModel } from './animal.model';

export class ConsultationModel {
  id?: string;
  record?: string;
  date!: Date;
  observations!: string;
  weight!: number;
  doctor!: UserModel;
  animal!: AnimalModel;
  vaccines!: VaccineModel[];
  diseases!: DiseaseModel[];
  cares!: CareModel[];
  created_at?: Date;
  updated_at?: Date;
}
