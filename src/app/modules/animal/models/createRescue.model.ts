import { CreateRescuer } from '../../user/models/create.rescuer.model';
import { UserModel } from '../../user/models/user.model';
import { AddressModel } from './address.model';
import { AnimalModel } from './animal.model';
import { CreateAddress } from './createAddress.model';
import { CreateAnimal } from './createAnimal.molde';

export class CreateRescue {
  user!: CreateRescuer;
  animal!: CreateAnimal;
  address!: CreateAddress;
}
