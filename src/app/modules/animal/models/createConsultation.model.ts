export class CreateConsultation {
  date!: Date;
  weight!: number;
  observations!: string;
  doctorId!: string;
  animalId!: string;
  vaccines!: [];
  diseases!: [];
  cares!: [];
}
