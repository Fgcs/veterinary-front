import { locationModel } from '../../maps/interfaces/location';

export class CreateAddress {
  street!: string;
  city!: string;
  state!: string;
  postal_code!: string;
  home!: string;
  reference!: string;
  location!: locationModel;
}
