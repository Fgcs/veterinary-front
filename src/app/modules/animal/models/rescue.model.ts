import { UserModel } from '../../user/models/user.model';
import { AnimalModel } from './animal.model';

export class RescueModel {
  id?: string;
  user!: UserModel;
  animal!: string;
  //address: Address;
  created_at?: Date;
  updated_at?: Date;
}
