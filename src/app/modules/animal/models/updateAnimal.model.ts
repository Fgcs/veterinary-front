export class UpdateAnimal {
  id?: string;
  name!: string;
  weight!: number;
  sex!: string;
  hair_color!: string;
  birthday!: Date;
  breedId!: string;
  specieId!: string;
}
