import { Component, Input, OnInit } from '@angular/core';
import { AnimalModel } from '../../models/animal.model';
import { AnimalService } from '../../services/animal.service';

@Component({
  selector: 'app-table-animal',
  templateUrl: './table-animal.component.html',
  styleUrls: ['./table-animal.component.css'],
})
export class TableAnimalComponent implements OnInit {
  animals: AnimalModel[] = [];
  p: number = 1;
  constructor(private animalService: AnimalService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.animalService.getAll().subscribe((data) => {
      this.animals = data;
    });
  }
  ondelete(id: string | undefined) {
    this.animalService.delete(id as string).subscribe((data) => {
      this.getAll();
    });
  }
}
