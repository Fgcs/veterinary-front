import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAnimalComponent } from './table-animal.component';

describe('TableAnimalComponent', () => {
  let component: TableAnimalComponent;
  let fixture: ComponentFixture<TableAnimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableAnimalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAnimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
