import { Component, Input, OnInit } from '@angular/core';
import { ConsultationModel } from '../../models/consultation.model';

@Component({
  selector: 'app-list-history',
  templateUrl: './list-history.component.html',
  styleUrls: ['./list-history.component.css'],
})
export class ListHistoryComponent implements OnInit {
  @Input() consultation: ConsultationModel | undefined;
  @Input() index: number | undefined;
  constructor() {}

  ngOnInit(): void {}
}
