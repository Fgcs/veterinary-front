import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdoptionComponent } from './components/adoption/adoption.component';
import { ClinicHistoryComponent } from './components/clinic-history/clinic-history.component';
import { ConsultationComponent } from './components/consultation/consultation.component';
import { CreateComponent } from './components/create/create.component';
import { DeleteComponent } from './components/delete/delete.component';
import { DetailsAnimalComponent } from './components/details-animal/details-animal.component';
import { EditAnimalComponent } from './components/edit-animal/edit-animal.component';
import { ListComponent } from './components/list/list.component';

const routes: Routes = [
  {
    path: 'agregar',
    component: CreateComponent,
  },
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'delete',
    component: DeleteComponent,
  },
  {
    path: 'adoption',
    component: AdoptionComponent,
  },
  {
    path: 'consultation/:id',
    component: ConsultationComponent,
  },
  {
    path: 'history',
    component: ClinicHistoryComponent,
  },
  {
    path: 'editar/:id',
    component: EditAnimalComponent,
  },
  {
    path: 'detalles/:id',
    component: DetailsAnimalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnimalRoutingModule {}
