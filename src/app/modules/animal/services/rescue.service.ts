import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateRescue } from '../models/createRescue.model';
import { RescueModel } from '../models/rescue.model';

@Injectable({
  providedIn: 'root',
})
export class RescueService {
  constructor(private http: HttpClient) {}
  create(rescue: CreateRescue): Observable<RescueModel> {
    return this.http.post<RescueModel>(`${environment.apiUrl}/rescue`, rescue);
  }
  getAll(): Observable<RescueModel[]> {
    return this.http.get<RescueModel[]>(`${environment.apiUrl}/rescue`);
  }
  getById(id: string): Observable<RescueModel> {
    return this.http.get<RescueModel>(`${environment.apiUrl}/rescue/${id}`);
  }
  delete(id: string): Observable<RescueModel> {
    return this.http.delete<RescueModel>(`${environment.apiUrl}/rescue/${id}`);
  }
  update(id: string, rescue: CreateRescue): Observable<RescueModel> {
    return this.http.patch<RescueModel>(
      `${environment.apiUrl}/rescue/${id}`,
      rescue
    );
  }
}
