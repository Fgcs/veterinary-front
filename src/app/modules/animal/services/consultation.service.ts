import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ConsultationModel } from '../models/consultation.model';
import { CreateConsultation } from '../models/createConsultation.model';

@Injectable({
  providedIn: 'root',
})
export class ConsultationService {
  constructor(private http: HttpClient) {}
  create(animal: CreateConsultation): Observable<ConsultationModel> {
    return this.http.post<ConsultationModel>(
      `${environment.apiUrl}/consultation`,
      animal
    );
  }
  getAll(): Observable<ConsultationModel[]> {
    return this.http.get<ConsultationModel[]>(
      `${environment.apiUrl}/consultation`
    );
  }
  getById(id: string): Observable<ConsultationModel> {
    return this.http.get<ConsultationModel>(
      `${environment.apiUrl}/consultation/${id}`
    );
  }
  getByAnimalId(id: string): Observable<ConsultationModel[]> {
    return this.http.get<ConsultationModel[]>(
      `${environment.apiUrl}/consultation/animal/${id}`
    );
  }
  delete(id: string): Observable<ConsultationModel> {
    return this.http.delete<ConsultationModel>(
      `${environment.apiUrl}/consultation/${id}`
    );
  }
  update(
    id: string,
    animal: CreateConsultation
  ): Observable<ConsultationModel> {
    return this.http.patch<ConsultationModel>(
      `${environment.apiUrl}/consultation/${id}`,
      animal
    );
  }
}
