import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AnimalModel } from '../models/animal.model';
import { CreateAnimal } from '../models/createAnimal.molde';
import { UpdateAnimal } from '../models/updateAnimal.model';

@Injectable({
  providedIn: 'root',
})
export class AnimalService {
  constructor(private http: HttpClient) {}
  create(animal: CreateAnimal): Observable<AnimalModel> {
    return this.http.post<AnimalModel>(`${environment.apiUrl}/animal`, animal);
  }
  getAll(): Observable<AnimalModel[]> {
    return this.http.get<AnimalModel[]>(`${environment.apiUrl}/animal`);
  }
  getById(id: string): Observable<AnimalModel> {
    return this.http.get<AnimalModel>(`${environment.apiUrl}/animal/${id}`);
  }
  delete(id: string): Observable<AnimalModel> {
    return this.http.delete<AnimalModel>(`${environment.apiUrl}/animal/${id}`);
  }
  update(animal: UpdateAnimal): Observable<AnimalModel> {
    return this.http.patch<AnimalModel>(
      `${environment.apiUrl}/animal/${animal.id}`,
      animal
    );
  }
}
