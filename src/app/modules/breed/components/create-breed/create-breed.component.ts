import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateBreed } from '../../models/createBreed.model';
import { BreedService } from '../../services/breed.service';

@Component({
  selector: 'app-create-breed',
  templateUrl: './create-breed.component.html',
  styleUrls: ['./create-breed.component.css'],
})
export class CreateBreedComponent implements OnInit {
  breedForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private breedService: BreedService,
    private sweetAlert: SweetAlertService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  get f() {
    return this.breedForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.breedForm.valid) {
      const breed: CreateBreed = this.breedForm.value;
      this.breedService.create(breed).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Raza Guardado correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/razas/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar esta raza'
          );
        }
      );
    }
  }
}
