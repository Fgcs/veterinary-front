import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { BreedModel } from '../../models/breed.model';
import { UpdateBreed } from '../../models/updateBreed.model';
import { BreedService } from '../../services/breed.service';

@Component({
  selector: 'app-edit-breed',
  templateUrl: './edit-breed.component.html',
  styleUrls: ['./edit-breed.component.css'],
})
export class EditBreedComponent implements OnInit {
  breedForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
  });
  submitted: boolean = false;
  breedId: string = '';
  breed!: BreedModel;
  constructor(
    private fb: FormBuilder,
    private breedService: BreedService,
    private sweetAlert: SweetAlertService,
    private router: Router,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.breedId = this.activated.snapshot.paramMap.get('id') as string;
    this.getBreedById();
  }
  getBreedById() {
    this.breedService.getById(this.breedId).subscribe((dta) => {
      this.breed = dta;
      this.setForm();
    });
  }
  setForm() {
    this.breedForm.setValue({
      name: this.breed.name,
    });
  }
  get f() {
    return this.breedForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.breedForm.valid) {
      const breed: UpdateBreed = this.breedForm.value;
      this.breedService.update(this.breedId, breed).subscribe(
        (data) => {
          this.sweetAlert
            .succesAlert('Raza Guardado correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/razas/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar esta raza'
          );
        }
      );
    }
  }
}
