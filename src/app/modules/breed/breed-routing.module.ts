import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateBreedComponent } from './components/create-breed/create-breed.component';
import { EditBreedComponent } from './components/edit-breed/edit-breed.component';
import { ListBreedComponent } from './components/list-breed/list-breed.component';

const routes: Routes = [
  {
    path: 'listado',
    component: ListBreedComponent,
  },
  {
    path: 'agregar',
    component: CreateBreedComponent,
  },
  {
    path: 'editar/:id',
    component: EditBreedComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BreedRoutingModule {}
