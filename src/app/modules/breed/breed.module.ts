import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BreedRoutingModule } from './breed-routing.module';
import { CreateBreedComponent } from './components/create-breed/create-breed.component';
import { EditBreedComponent } from './components/edit-breed/edit-breed.component';
import { ListBreedComponent } from './components/list-breed/list-breed.component';
import { TableBreedComponent } from './shared/table-breed/table-breed.component';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { RouterModule } from '@angular/router';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    CreateBreedComponent,
    EditBreedComponent,
    ListBreedComponent,
    TableBreedComponent,
  ],
  imports: [
    CommonModule,
    BreedRoutingModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    ReactiveFormsModule,
    FormsModule, NgxPaginationModule
  ],
})
export class BreedModule { }
