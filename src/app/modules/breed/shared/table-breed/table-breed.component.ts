import { Component, OnInit } from '@angular/core';
import { BreedModel } from '../../models/breed.model';
import { BreedService } from '../../services/breed.service';

@Component({
  selector: 'app-table-breed',
  templateUrl: './table-breed.component.html',
  styleUrls: ['./table-breed.component.css'],
})
export class TableBreedComponent implements OnInit {
  breeds: BreedModel[] = [];
  p: number = 1;
  constructor(private breedService: BreedService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.breedService.getAll().subscribe((data) => (this.breeds = data));
  }
  ondelete(id: string | undefined) {
    this.breedService.delete(id as string).subscribe((data) => {
      this.getAll();
    });
  }
}
