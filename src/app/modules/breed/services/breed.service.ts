import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BreedModel } from '../models/breed.model';
import { CreateBreed } from '../models/createBreed.model';
import { UpdateBreed } from '../models/updateBreed.model';

@Injectable({
  providedIn: 'root',
})
export class BreedService {
  constructor(private http: HttpClient) {}
  create(breed: CreateBreed): Observable<BreedModel> {
    return this.http.post<BreedModel>(`${environment.apiUrl}/breed`, breed);
  }
  getAll(): Observable<BreedModel[]> {
    return this.http.get<BreedModel[]>(`${environment.apiUrl}/breed`);
  }
  getById(id: string): Observable<BreedModel> {
    return this.http.get<BreedModel>(`${environment.apiUrl}/breed/${id}`);
  }
  delete(id: string): Observable<BreedModel> {
    return this.http.delete<BreedModel>(`${environment.apiUrl}/breed/${id}`);
  }
  update(id: string, breed: UpdateBreed): Observable<BreedModel> {
    return this.http.patch<BreedModel>(
      `${environment.apiUrl}/breed/${id}`,
      breed
    );
  }
  foundBreedByName(name: string): Observable<BreedModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<BreedModel[]>(
      `${environment.apiUrl}/breed/find-by-name?name=${name}`,
      { headers }
    );
  }
}
