import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateVaccine } from '../models/createVaccine.model';
import { UpdateVaccine } from '../models/updateVaccine.model';
import { VaccineModel } from '../models/vaccine.model';

@Injectable({
  providedIn: 'root',
})
export class VaccineService {
  constructor(private http: HttpClient) {}

  create(vaccine: CreateVaccine): Observable<VaccineModel> {
    return this.http.post<VaccineModel>(
      `${environment.apiUrl}/vaccine`,
      vaccine
    );
  }
  getAll(): Observable<VaccineModel[]> {
    return this.http.get<VaccineModel[]>(`${environment.apiUrl}/vaccine`);
  }
  getById(id: string): Observable<VaccineModel> {
    return this.http.get<VaccineModel>(`${environment.apiUrl}/vaccine/${id}`);
  }
  delete(id: string): Observable<VaccineModel> {
    return this.http.delete<VaccineModel>(
      `${environment.apiUrl}/vaccine/${id}`
    );
  }
  update(id: string, vaccine: UpdateVaccine): Observable<VaccineModel> {
    return this.http.patch<VaccineModel>(
      `${environment.apiUrl}/vaccine/${id}`,
      vaccine
    );
  }
  foundVaccineByName(name: string): Observable<VaccineModel[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<VaccineModel[]>(
      `${environment.apiUrl}/vaccine/search/find-by-name?name=${name}`,
      { headers }
    );
  }
}
