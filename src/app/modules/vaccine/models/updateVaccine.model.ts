export class UpdateVaccine {
  name!: string;
  description!: string;
  manufacturer!: string;
  via!: string;
}
