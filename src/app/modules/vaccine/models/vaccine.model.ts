export class VaccineModel {
  id?: string;
  name?: string;
  description?: string;
  manufacturer?: string;
  via?: string;
  type?: string;
  created_at?: Date;
  updated_at?: Date;
}
