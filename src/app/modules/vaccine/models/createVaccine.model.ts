export class CreateVaccine {
  name!: string;
  description!: string;
  manufacturer!: string;
  via!: string;
}
