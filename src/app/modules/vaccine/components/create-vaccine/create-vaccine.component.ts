import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { CreateVaccine } from '../../models/createVaccine.model';
import { VaccineService } from '../../services/vaccine.service';

@Component({
  selector: 'app-create-vaccine',
  templateUrl: './create-vaccine.component.html',
  styleUrls: ['./create-vaccine.component.css'],
})
export class CreateVaccineComponent implements OnInit {
  vaccineForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    manufacturer: ['', Validators.required],
    via: [null, Validators.required],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private vaccineService: VaccineService,
    private sweetAlert: SweetAlertService
  ) {}

  ngOnInit(): void {}
  get f() {
    return this.vaccineForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.vaccineForm.valid) {
      const body: CreateVaccine = this.vaccineForm.value;
      this.vaccineService.create(body).subscribe(
        (resp) => {
          this.sweetAlert
            .succesAlert('Vacuna Guarda correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/vacunas/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la vacuna'
          );
        }
      );
    }
  }
}
