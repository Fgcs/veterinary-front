import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { UpdateVaccine } from '../../models/updateVaccine.model';
import { VaccineModel } from '../../models/vaccine.model';
import { VaccineService } from '../../services/vaccine.service';

@Component({
  selector: 'app-edit-vaccine',
  templateUrl: './edit-vaccine.component.html',
  styleUrls: ['./edit-vaccine.component.css'],
})
export class EditVaccineComponent implements OnInit {
  vaccineForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    manufacturer: ['', Validators.required],
    via: [null, Validators.required],
  });
  submitted: boolean = false;
  vaccine!: VaccineModel;
  vaccineId: string = '';
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private vaccineService: VaccineService,
    private sweetAlert: SweetAlertService,
    private activated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.vaccineId = this.activated.snapshot.paramMap.get('id') as string;
    this.getVaccineById();
  }
  getVaccineById() {
    this.vaccineService.getById(this.vaccineId).subscribe((data) => {
      this.vaccine = data;
      this.setData();
    });
  }
  setData() {
    this.vaccineForm.setValue({
      name: this.vaccine.name,
      description: this.vaccine.description,
      manufacturer: this.vaccine.manufacturer,
      via: this.vaccine.via,
    });
  }
  get f() {
    return this.vaccineForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.vaccineForm.valid) {
      const body: UpdateVaccine = this.vaccineForm.value;
      this.vaccineService.update(this.vaccine.id as string, body).subscribe(
        (resp) => {
          this.sweetAlert
            .succesAlert('Vacuna Guarda correctamente')
            .then((end) => {
              this.router.navigateByUrl('/dashboard/vacunas/listado');
            });
        },
        (err) => {
          this.sweetAlert.errorAlert(
            'A ocurrido un error al guardar la vacuna'
          );
        }
      );
    }
  }
}
