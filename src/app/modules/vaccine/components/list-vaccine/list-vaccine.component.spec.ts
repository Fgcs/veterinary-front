import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVaccineComponent } from './list-vaccine.component';

describe('ListVaccineComponent', () => {
  let component: ListVaccineComponent;
  let fixture: ComponentFixture<ListVaccineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListVaccineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVaccineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
