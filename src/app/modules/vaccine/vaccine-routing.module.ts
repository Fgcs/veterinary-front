import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateVaccineComponent } from './components/create-vaccine/create-vaccine.component';
import { EditVaccineComponent } from './components/edit-vaccine/edit-vaccine.component';
import { ListVaccineComponent } from './components/list-vaccine/list-vaccine.component';

const routes: Routes = [
  {
    path: 'listado',
    component: ListVaccineComponent,
  },
  {
    path: 'agregar',
    component: CreateVaccineComponent,
  },
  {
    path: 'editar/:id',
    component: EditVaccineComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VaccineRoutingModule {}
