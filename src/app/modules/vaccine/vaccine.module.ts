import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VaccineRoutingModule } from './vaccine-routing.module';
import { ListVaccineComponent } from './components/list-vaccine/list-vaccine.component';
import { CreateVaccineComponent } from './components/create-vaccine/create-vaccine.component';
import { EditVaccineComponent } from './components/edit-vaccine/edit-vaccine.component';
import { TitlePageModule } from '../shared/title-page/title-page.module';
import { TableVaccineComponent } from './shared/table-vaccine/table-vaccine.component';
import { RouterModule } from '@angular/router';
import { DeleteModule } from '../shared/buttons/delete/delete.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    ListVaccineComponent,
    CreateVaccineComponent,
    EditVaccineComponent,
    TableVaccineComponent,
  ],
  imports: [
    CommonModule,
    VaccineRoutingModule,
    TitlePageModule,
    RouterModule,
    DeleteModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
})
export class VaccineModule { }
