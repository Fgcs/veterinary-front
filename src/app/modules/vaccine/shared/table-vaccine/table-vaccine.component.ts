import { Component, Input, OnInit } from '@angular/core';
import { VaccineModel } from '../../models/vaccine.model';
import { VaccineService } from '../../services/vaccine.service';

@Component({
  selector: 'app-table-vaccine',
  templateUrl: './table-vaccine.component.html',
  styleUrls: ['./table-vaccine.component.css'],
})
export class TableVaccineComponent implements OnInit {
  @Input() vaccines: VaccineModel[] = [];
  p: number = 1;
  constructor(private vaccineService: VaccineService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.vaccineService.getAll().subscribe((resp) => {
      this.vaccines = resp;
    });
  }
  ondelete(id: string | undefined) {
    this.vaccineService.delete(id as string).subscribe((data) => {
      this.getAll();
    });
  }
}
