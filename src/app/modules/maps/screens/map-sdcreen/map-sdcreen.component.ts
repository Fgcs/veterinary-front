import { Component, EventEmitter, Output } from '@angular/core';
import { locationModel } from '../../interfaces/location';
import { PlacesService } from '../../services';

@Component({
  selector: 'app-map-sdcreen',
  templateUrl: './map-sdcreen.component.html',
  styleUrls: ['./map-sdcreen.component.css'],
})
export class MapSDcreenComponent {
  @Output() locationEvent = new EventEmitter<locationModel>();
  constructor(private placesService: PlacesService) {}
  get isUserLocationReady() {
    return this.placesService.isUserLocationReady;
  }
  sendLocation(location: locationModel) {
    this.locationEvent.emit(location);
  }
}
