import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapSDcreenComponent } from './screens/map-sdcreen/map-sdcreen.component';
import { LoadingComponent } from './components/loading/loading.component';
import { MapViewComponent } from './components/map-view/map-view.component';
import { BtnMyLocationComponent } from './components/btn-my-location/btn-my-location.component';
import { LogoComponent } from './components/logo/logo.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';

@NgModule({
  declarations: [
    MapSDcreenComponent,
    LoadingComponent,
    MapViewComponent,
    BtnMyLocationComponent,
    LogoComponent,
    SearchBarComponent,
    SearchResultsComponent,
  ],
  imports: [CommonModule],
  exports: [MapSDcreenComponent],
})
export class MapsModule {}
