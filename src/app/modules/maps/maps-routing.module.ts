import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapSDcreenComponent } from './screens/map-sdcreen/map-sdcreen.component';

const routes: Routes = [
  {
    path: 'maps',
    component: MapSDcreenComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapsRoutingModule {}
