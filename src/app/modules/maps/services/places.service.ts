import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  public useLocation?: [number,number];
  
  get isUserLocationReady(): boolean{
    return!!this.useLocation;
  }
  constructor(private http: HttpClient ) { 
    this.getUserLocation();
  }

  public async getUserLocation(): Promise<[number,number]>{
    return new Promise( (resolve,reject) =>{
      navigator.geolocation.getCurrentPosition(
        ({coords})=>{
          this.useLocation = [coords.longitude,coords.latitude];
          resolve(this.useLocation);
        },
        (err)=>{
          alert('No se pudo obtener la geolocalización');
          console.log(err);
          reject();
        }
      )
    } 

    )
  }

  getPlacesByQuery(query:string = ''){
    //Todo:evaluar cuando el query es nulo

    this.http.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?limit=5&proximity=-66.96268446099832%2C10.37435663459533&language=es&access_token=YOUR_MAPBOX_ACCESS_TOKEN`)
     .subscribe(console.log);
  }
}
