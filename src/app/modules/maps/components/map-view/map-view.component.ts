import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { Map, Popup, Marker } from 'mapbox-gl';
import { locationModel } from '../../interfaces/location';
import { MapService, PlacesService } from '../../services';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css'],
})
export class MapViewComponent implements AfterViewInit {
  @Output() locationEvent = new EventEmitter<locationModel>();
  constructor(
    private placesService: PlacesService,
    private mapService: MapService
  ) {}

  @ViewChild('mapDiv')
  mapDivElement!: ElementRef;

  ngAfterViewInit(): void {
    if (!this.placesService.useLocation) throw Error('No hay placesServices');

    const map = new Map({
      container: this.mapDivElement.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11', // style URL
      center: this.placesService.useLocation, // starting position [lng, lat]
      zoom: 13, // starting zoom
    });

    const popup = new Popup().setHTML(`
          <h6> Aqui estoy chiguire</h6>
          <span>Estoy en este lugar del mundo</span>
        `);
    const body: locationModel = {
      latitude: String(this.placesService.useLocation[1]),
      longitude: String(this.placesService.useLocation[0]),
    };
    this.locationEvent.emit(body);
    const marker = new Marker({ draggable: true, color: 'red' })
      .setLngLat(this.placesService.useLocation)
      .setPopup(popup)
      .addTo(map);

    marker.on('dragend', async () => {
      const location = await marker.getLngLat();
      const body: locationModel = {
        latitude: String(location.lat),
        longitude: String(location.lng),
      };
      this.locationEvent.emit(body);
    });
    this.mapService.setMap(map);
  }
}
