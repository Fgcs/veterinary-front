import { UserModel } from '../../user/models/user.model';

export class AuthModel {
  id?: string;

  dateLogin?: Date;

  tokenType?: string;

  token?: string;

  expiresIn?: number;
  user?: UserModel;
}
