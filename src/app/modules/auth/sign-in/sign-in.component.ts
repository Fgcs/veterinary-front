import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { UserModel } from '../../user/models/user.model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    remember: [false],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private sweet: SweetAlertService
  ) {}

  ngOnInit(): void {
    this.isRememberEmail();
  }
  get f() {
    return this.loginForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      const { remember, email, password } = this.loginForm.value;
      this.authService.login({ email, password }).subscribe(
        (data) => {
          this.router.navigateByUrl('/dashboard');
          remember && localStorage.setItem('email', email);
          this.authService.saveToken(data.token as string);
          this.authService.saveUser(data.user as UserModel);
          this.authService.updateAuth(false);
        },
        (err) => {
          console.log(err, 'err');
          this.sweet.errorAlert('Correo electronico o contraseña incorrecta.');
        }
      );
    }
  }
  isRememberEmail() {
    if (localStorage.getItem('email')) {
      const email = localStorage.getItem('email');
      this.loginForm.patchValue({ email: email, remember: true });
    }
  }
}
