import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthModel } from '../models/auth.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginModel } from '../models/login.model';
import { environment } from 'src/environments/environment';
import { CreateUser } from '../../user/models/createUser.model';
import { UserModel } from '../../user/models/user.model';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuth = new BehaviorSubject<boolean>(true);
  constructor(private http: HttpClient) {}

  login(login: LoginModel): Observable<AuthModel> {
    return this.http.post<AuthModel>(`${environment.apiUrl}/auth/login`, login);
  }
  register(user: CreateUser): Observable<UserModel> {
    return this.http.post<UserModel>(
      `${environment.apiUrl}/auth/register`,
      user
    );
  }
  get isAuth$(): Observable<boolean> {
    return this.isAuth.asObservable();
  }
  signOut() {
    this.authComplete(true);
    this.deleteToken();
  }
  saveToken(token: string) {
    localStorage.setItem('token', token);
  }
  readToken() {
    let token;
    if (localStorage.getItem('token')) {
      token = localStorage.getItem('token');
    }
    return token;
  }
  verifyAuth() {
    if (localStorage.getItem('token')) {
      this.authComplete(false);
    } else {
      this.authComplete(true);
    }
  }
  saveUser(user: UserModel) {
    localStorage.setItem('user', JSON.stringify(user));
  }
  getUser() {
    return JSON.parse(localStorage.getItem('user') as string);
  }
  hasAuth() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }
  updateAuth(value: boolean) {
    this.authComplete(value);
  }
  private authComplete(value: boolean): void {
    this.isAuth.next(value);
  }
  deleteToken() {
    localStorage.removeItem('token');
  }
}
