import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root',
})
export class SweetAlertService {
  constructor() {}
  async deleteAlert(text: string) {
    return await Swal.fire({
      title: text,
      text: 'No podrás revertir esto!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, Borrar!',
    });
  }
  async succesAlert(text: string) {
    return await Swal.fire({
      position: 'center',
      icon: 'success',
      title: text,
      showConfirmButton: false,
      timer: 1500,
    });
  }
  async errorAlert(text: string) {
    return await Swal.fire({
      position: 'center',
      icon: 'error',
      title: text,
      showConfirmButton: true,
    });
  }
}
