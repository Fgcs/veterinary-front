import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  ValidationErrors,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class UsernameValidatorService implements AsyncValidator {
  constructor(private http: HttpClient) {}
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    const username = control.value;
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http
      .get<any>(
        `${environment.apiUrl}/user/search/find-by-username?username=${username}`,
        { headers }
      )
      .pipe(
        map((resp) => {
          return resp.length > 0 ? { usernameFound: true } : null;
        })
      );
  }
}
