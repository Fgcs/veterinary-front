import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  ValidationErrors,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class DniValidatorService implements AsyncValidator {
  constructor(private http: HttpClient) {}

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    const dni = control.value;
    return this.http
      .get<any>(`${environment.apiUrl}/user/search/find-by-dni?dni=${dni}`, {
        headers,
      })
      .pipe(
        map((resp) => {
          return resp.length > 0 ? { dniFound: true } : null;
        })
      );
  }
}
