import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SpinnerService } from '../services/spinner.service';
@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
  constructor(private spinnetSvc: SpinnerService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.headers.get('InterceptorSkipHeader') !== null) {
      return next.handle(req);
    } else {
      this.spinnetSvc.show();
      return next.handle(req).pipe(finalize(() => this.spinnetSvc.hide()));
    }
  }
}
