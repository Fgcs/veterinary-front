import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Mapboxgl from 'mapbox-gl'; // or "const mapboxgl = require('mapbox-gl');"
 
Mapboxgl.accessToken = 'pk.eyJ1Ijoia2xldmVyZ3oiLCJhIjoiY2wxNXU2YjNwMHcwMDNpdjBwNmhlcmR0cSJ9.eUAcrvoA52Maohz_HJ5XJA';


if (environment.production) {
  enableProdMode();
}

if(!navigator.geolocation){
  alert('Navegador no soporta la geolocalización');
  throw new Error('Navagador no soporta la geolocalización');
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
