export const environment = {
  production: true,
  apiUrl: 'https://rescue-veterinary.onrender.com/api',
};
